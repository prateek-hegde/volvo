require('./config/setup');

require('./services/database')

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');

var app = express();

const PORT = 80;

app.use(helmet());
app.use(cors());

app.use(morgan('dev'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Setting headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    //Request headers
    res.setHeader('Access-control-Allow-Headers', 'Content-Type, Access-control-Allow-Headers, Authorization');

    next();
});

//Client API's
const routes = require('./api/routes/router');
app.use('/api/',routes);


// Catch 404
app.use((req, res, next) => {
    res.status(404).send({
        error: "404 : Page not found"
    });
});

// Error handler
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    if (error.status != 500) {
        error = error.message;
    }
    res.send({
        error: error
    });
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

module.exports = app;