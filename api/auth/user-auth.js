const config = require('../../config/setup');
const User = require('../models/users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const encryptPassword = async (password) => {
    try {
        let salt = await bcrypt.genSalt(10);
        let hash = await bcrypt.hash(password, salt);
        return hash;
    } catch (error) {
        console.log(error);
    }
}

const generateAccessToken = async (user) => {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 30);

    var token = await jwt.sign({
        _id: user._id,
        name: user.userName,
        team: user.team,
        exp: parseInt(expiry.getTime() / 1000),
    }, config.secreteKey);

    return token;

};

const generateRefreshToken = async (user) => {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);

    var token = await jwt.sign({
        _id: user._id,
        email: user.email,
        name: user.firstName,
        exp: parseInt(expiry.getTime() / 1000),
    }, config.secreteKey2);

    return token;

};


module.exports.login = async (req, res) => {
    var userName = req.body.userName;
    var password = req.body.password;
    var email = req.body.email;

    try {
        var user = await User.findOne({$or: [{userName: userName}, {email:email}]});
    } catch (error) {
        console.log(error);
    }

    if (user === null) {
        return res.send("User does not exist");
    }

    let hash = user.password;
    try {
        var result = await bcrypt.compare(password, hash);
    } catch (error) {
        console.log(error);
    }


    if (result === false) {
        return res.send("Wrong Password");
    }


    //Email Verification
    // if(user.emailConfirmation === false){
    //     return res.send({
    //         message: "Please verify your email address"
    //     });
    // }

    try {
        let token = await generateAccessToken(user);
        return res.send(token);
    } catch (error) {
        console.log(error);

    }


};

module.exports.register = async (req, res) => {

    var userName = req.body.userName;
    var email = req.body.email;



    try {
        userExisted = await User.findOne({ userName: userName });
    } catch (error) {
        console.log(error);
    }

    if (userExisted !== null) {
        return res.send("User already exists!");
    }


    var hash = await encryptPassword(req.body.password);

    var userObject = {
        userName: userName,
        email: email,
        password: hash
    };

    try {
        var user = await User.create(userObject);
    } catch (error) {
        console.log(error);
    }

    if (user) {
        return res.send("Registered Successfully!");
    } else {
        return res.send("Something went wrong");
    }




};
