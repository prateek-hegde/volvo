const User = require('../models/users');
const {ObjectID} = require('mongodb');

module.exports.ourTeam = async(req, res) => {
    var team = req.decoded.team;

    try {
        var users = await User.find({team: team},{"password": 0, "team": 0, "tickets": 0});
    } catch (error) {
        console.log(error);
    }

    if(users) {
 
        return res.json({
            result: users
        });
    }
}

module.exports.getTickets = async (req, res) => {
    var userId = req.decoded._id;

    try {
        var user = await User.findOne({_id: ObjectID(userId)});
    } catch (error) {
        console.log(error);
    }

    if(user){
        var tickets = user.tickets;
        return res.json({
            result: tickets
        });
    }

}