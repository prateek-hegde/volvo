var mongoose = require('mongoose');

var UsersSchema = new mongoose.Schema({
userName: {
    type: String,
    required: true
},
email: {
    type: String,
    required: true 
},
password: {
    type: String,
    required: true
},
image: {
    type: String,
},
email: {
    type: String,
    required: true
},
phone: {
    type: Number,
},
team: {
    type: String,
},
tickets: {
    type: Array,
    default: []
}

});

var User = mongoose.model('User', UsersSchema);
module.exports = User;
