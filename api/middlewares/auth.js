const jwt = require('jsonwebtoken')
const config = require('../../config/setup')

module.exports = async (req,res,next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization']


   if(token){

        try {
            var decoded = await jwt.verify(token, config.secreteKey);
            req.decoded = decoded;
            next();

        } catch (error) {
            return res.status(401).json({"error": true, "message": 'Unauthorized access.' });
        }

    } else {

        return res.status(403).send({
            "error": true,
            "message": 'No token provided.'
        });


    }
}