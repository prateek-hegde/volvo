const express = require('express');
var router = express();

const auth = require('../auth/user-auth');
const tokenValidator = require('../middlewares/auth');
const userController = require('../controllers/user-controller');

//User Authentication
router.post('/login', auth.login);
router.post('/register', auth.register);

router.get('/ourteam', 
    tokenValidator, userController.ourTeam);

router.get('/tickets',
    tokenValidator, userController.getTickets);

module.exports = router;